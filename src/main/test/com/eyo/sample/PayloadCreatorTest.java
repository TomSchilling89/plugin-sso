package com.eyo.sample;

import com.eyo.sample.model.SSOPayload;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Date;

public class PayloadCreatorTest {
    private final PayloadCreator unitUnderTest = new PayloadCreator();
    private JwtClaims correctClaims;
    private JwtClaims wrongFormatClaims;

    /**
     * Responsible for mocking the {@link JwtClaims} so that it returns certain expected values.
     * @throws MalformedClaimException should not be thrown
     */
    @Before
    public void init() throws MalformedClaimException {
        correctClaims = Mockito.spy(JwtClaims.class);
        Mockito.when(correctClaims.getClaimValue("eyo.i", String.class)).thenReturn("TestInstance");
        Mockito.when(correctClaims.getClaimValue("eyo.u", String.class)).thenReturn("TestUser");
        Mockito.when(correctClaims.getClaimValue("eyo.ue", String.class)).thenReturn("TestUserExternal");
        Mockito.when(correctClaims.getClaimValue("eyo.ufn", String.class)).thenReturn("TestUserFirstName");
        Mockito.when(correctClaims.getClaimValue("eyo.uln", String.class)).thenReturn("TesrtUserLastName");
        Mockito.when(correctClaims.getClaimValue("eyo.ur", String.class)).thenReturn("TestUserRole");
        Mockito.when(correctClaims.getClaimValue("eyo.ul", String.class)).thenReturn("TestUserLocale");

        //FIXME: Testing against wrongful formatting would only work if the unitUnderTest would not cast to String automatically.
//        wrongFormatClaims = Mockito.spy(JwtClaims.class);
//        Mockito.when(wrongFormatClaims.getClaimValue("eyo.i")).thenThrow(new MalformedClaimException("Was a date instead of a String."));
//        Mockito.when(wrongFormatClaims.getClaimValue("eyo.u")).thenReturn("TestUser");
//        Mockito.when(wrongFormatClaims.getClaimValue("eyo.ue")).thenReturn("TestUserExternal");
//        Mockito.when(wrongFormatClaims.getClaimValue("eyo.ufn")).thenReturn("TestUserFirstName");
//        Mockito.when(wrongFormatClaims.getClaimValue("eyo.uln")).thenReturn("TesrtUserLastName");
//        Mockito.when(wrongFormatClaims.getClaimValue("eyo.ur")).thenReturn("TestUserRole");
//        Mockito.when(wrongFormatClaims.getClaimValue("eyo.ul")).thenReturn("TestUserLocale");
    }

    @Test
    public void testForCorrectClaims() throws MalformedClaimException {
        SSOPayload payload = unitUnderTest.createPayload(correctClaims);
        Assert.assertEquals("TestInstance", payload.getInstanceID());
        Assert.assertEquals("TestUser", payload.getUserID());
        Assert.assertEquals("TestUserExternal", payload.getUserExternalID());
        Assert.assertEquals("TestUserFirstName", payload.getUserFirstName());
        Assert.assertEquals("TesrtUserLastName", payload.getUserLastName());
        Assert.assertEquals("TestUserRole", payload.getUserRole());
        Assert.assertEquals("TestUserLocale", payload.getUserLocale());
    }

//    @Test(expected = MalformedClaimException.class)
//    public void testForWrongFormatClaims() throws MalformedClaimException {
//        SSOPayload payload = unitUnderTest.createPayload(wrongFormatClaims);
//    }
}
