package com.eyo.sample;

import com.google.common.base.Charsets;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.InvalidJwtSignatureException;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.omg.CORBA.DynAnyPackage.Invalid;

public class SSOStringProcessorTest {
    private final static String AUDIENCE = "Testing";
    private final static String VALUE_SEPARATOR = "\":\"";
    private final static String ENTRY_SEPARATOR = "\",\"";
    private static final HmacKey KEY = new HmacKey("testtesttesttesttesttesttesttest".getBytes(Charsets.UTF_8));

    private SSOStringProcessor unitUnderTest;

    private String correctPayload;
    private String expiredDatePayload;
    private String wrongAudiencePayload;
    private String wrongKeyPayload;


    /**
     * Mocks PropertyProvider to return replicable Audience and Key contents for testing purposes
     */
    @Before
    public void mockPropertyProvider() {
        PropertyProvider propertyProvider = Mockito.mock(PropertyProvider.class);
        Mockito.when(propertyProvider.getPluginId()).thenReturn(AUDIENCE);
        Mockito.when(propertyProvider.getPluginKey()).thenReturn(KEY);

        unitUnderTest = new SSOStringProcessor(propertyProvider);
    }

    /**
     * Creates payload content and prepares it with {@link TestPayloadSerialiser} for usage in testing
     *
     * @throws JoseException
     */
    @Before
    public void initialisePayloads() throws JoseException {
        TestPayloadSerialiser testPayloadSerialiser = new TestPayloadSerialiser(KEY);
        String rawPayload = new StringBuilder().append("{\"")
                .append("aud").append(VALUE_SEPARATOR).append(AUDIENCE).append(ENTRY_SEPARATOR)
                .append("exp").append("\":").append("2481106805").append(",\"")
                .append("iat").append("\":").append("1481106745").append(",\"")
                .append("sub").append(VALUE_SEPARATOR).append("eyosso:584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("iss").append(VALUE_SEPARATOR).append("app.eyo.net").append(ENTRY_SEPARATOR)
                .append("eyo.i").append(VALUE_SEPARATOR).append("584694cae4b0fd010fa929c6").append(ENTRY_SEPARATOR)
                .append("eyo.u").append(VALUE_SEPARATOR).append("584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("eyo.t").append(VALUE_SEPARATOR).append("user").append(ENTRY_SEPARATOR)
                .append("eyo.ue").append(VALUE_SEPARATOR).append("tom").append(ENTRY_SEPARATOR)
                .append("eyo.ufn").append(VALUE_SEPARATOR).append("Tom").append(ENTRY_SEPARATOR)
                .append("eyo.uln").append(VALUE_SEPARATOR).append("Schilling").append(ENTRY_SEPARATOR)
                .append("eyo.ur").append(VALUE_SEPARATOR).append("editor").append(ENTRY_SEPARATOR)
                .append("eyo.ul").append(VALUE_SEPARATOR).append("de_DE").append("\"}").toString();
        correctPayload = testPayloadSerialiser.serialise(rawPayload);

        rawPayload = new StringBuilder().append("{\"")
                .append("aud").append(VALUE_SEPARATOR).append(AUDIENCE).append(ENTRY_SEPARATOR)
                .append("exp").append("\":").append("1481106805").append(",\"")
                .append("iat").append("\":").append("1481106745").append(",\"")
                .append("sub").append(VALUE_SEPARATOR).append("eyosso:584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("iss").append(VALUE_SEPARATOR).append("app.eyo.net").append(ENTRY_SEPARATOR)
                .append("eyo.i").append(VALUE_SEPARATOR).append("584694cae4b0fd010fa929c6").append(ENTRY_SEPARATOR)
                .append("eyo.u").append(VALUE_SEPARATOR).append("584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("eyo.t").append(VALUE_SEPARATOR).append("user").append(ENTRY_SEPARATOR)
                .append("eyo.ue").append(VALUE_SEPARATOR).append("tom").append(ENTRY_SEPARATOR)
                .append("eyo.ufn").append(VALUE_SEPARATOR).append("Tom").append(ENTRY_SEPARATOR)
                .append("eyo.uln").append(VALUE_SEPARATOR).append("Schilling").append(ENTRY_SEPARATOR)
                .append("eyo.ur").append(VALUE_SEPARATOR).append("editor").append(ENTRY_SEPARATOR)
                .append("eyo.ul").append(VALUE_SEPARATOR).append("de_DE").append("\"}").toString();
        expiredDatePayload = testPayloadSerialiser.serialise(rawPayload);

        rawPayload = new StringBuilder().append("{\"")
                .append("aud").append(VALUE_SEPARATOR).append("SOME_OTHER_AUDIENCE").append(ENTRY_SEPARATOR)
                .append("exp").append("\":").append("2481106805").append(",\"")
                .append("iat").append("\":").append("1481106745").append(",\"")
                .append("sub").append(VALUE_SEPARATOR).append("eyosso:584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("iss").append(VALUE_SEPARATOR).append("app.eyo.net").append(ENTRY_SEPARATOR)
                .append("eyo.i").append(VALUE_SEPARATOR).append("584694cae4b0fd010fa929c6").append(ENTRY_SEPARATOR)
                .append("eyo.u").append(VALUE_SEPARATOR).append("584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("eyo.t").append(VALUE_SEPARATOR).append("user").append(ENTRY_SEPARATOR)
                .append("eyo.ue").append(VALUE_SEPARATOR).append("tom").append(ENTRY_SEPARATOR)
                .append("eyo.ufn").append(VALUE_SEPARATOR).append("Tom").append(ENTRY_SEPARATOR)
                .append("eyo.uln").append(VALUE_SEPARATOR).append("Schilling").append(ENTRY_SEPARATOR)
                .append("eyo.ur").append(VALUE_SEPARATOR).append("editor").append(ENTRY_SEPARATOR)
                .append("eyo.ul").append(VALUE_SEPARATOR).append("de_DE").append("\"}").toString();
        wrongAudiencePayload = testPayloadSerialiser.serialise(rawPayload);

        rawPayload = new StringBuilder().append("{\"")
                .append("aud").append(VALUE_SEPARATOR).append(AUDIENCE).append(ENTRY_SEPARATOR)
                .append("exp").append("\":").append("2481106805").append(",\"")
                .append("iat").append("\":").append("1481106745").append(",\"")
                .append("sub").append(VALUE_SEPARATOR).append("eyosso:584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("iss").append(VALUE_SEPARATOR).append("app.eyo.net").append(ENTRY_SEPARATOR)
                .append("eyo.i").append(VALUE_SEPARATOR).append("584694cae4b0fd010fa929c6").append(ENTRY_SEPARATOR)
                .append("eyo.u").append(VALUE_SEPARATOR).append("584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("eyo.t").append(VALUE_SEPARATOR).append("user").append(ENTRY_SEPARATOR)
                .append("eyo.ue").append(VALUE_SEPARATOR).append("tom").append(ENTRY_SEPARATOR)
                .append("eyo.ufn").append(VALUE_SEPARATOR).append("Tom").append(ENTRY_SEPARATOR)
                .append("eyo.uln").append(VALUE_SEPARATOR).append("Schilling").append(ENTRY_SEPARATOR)
                .append("eyo.ur").append(VALUE_SEPARATOR).append("editor").append(ENTRY_SEPARATOR)
                .append("eyo.ul").append(VALUE_SEPARATOR).append("de_DE").append("\"}").toString();
        testPayloadSerialiser = new TestPayloadSerialiser(new HmacKey("test1234test1234test1234test1234".getBytes(Charsets.UTF_8)));
        wrongKeyPayload = testPayloadSerialiser.serialise(rawPayload);
    }

    /**
     * Test to assert that all information is properly extracted into {@link JwtClaims#getClaimsMap()}
     *
     * @throws JoseException
     * @throws InvalidJwtException
     * @throws MalformedClaimException
     */
    @Test
    public void testForCorrectPayload() throws JoseException, InvalidJwtException, MalformedClaimException {
        JwtClaims claims = unitUnderTest.process(correctPayload);

        Assert.assertEquals("584694cae4b0fd010fa929c6", claims.getClaimValue("eyo.i", String.class));
        Assert.assertEquals("584694f6e4b0fd010fa929ca", claims.getClaimValue("eyo.u", String.class));
        Assert.assertEquals("user", claims.getClaimValue("eyo.t", String.class));
        Assert.assertEquals("tom", claims.getClaimValue("eyo.ue", String.class));
        Assert.assertEquals("Tom", claims.getClaimValue("eyo.ufn", String.class));
        Assert.assertEquals("Schilling", claims.getClaimValue("eyo.uln", String.class));
        Assert.assertEquals("editor", claims.getClaimValue("eyo.ur", String.class));
        Assert.assertEquals("de_DE", claims.getClaimValue("eyo.ul", String.class));
    }

    /**
     * Simple test to check whether the SSOStringProcessor appropriately fails when the wrong audience is presented
     *
     * @throws InvalidJwtException     is expected
     * @throws MalformedClaimException should not be thrown since it should throw earlier exception
     * @throws JoseException           should not be thrown since it should throw earlier exception
     */
    @Test
    public void testForWrongAudiencePayload() throws InvalidJwtException, MalformedClaimException, JoseException {
        try {
            JwtClaims claims = unitUnderTest.process(wrongAudiencePayload);
        } catch (InvalidJwtException e) {
            boolean isWrongAudience = e.getMessage().contains("Expected " + AUDIENCE + " as an aud value.");
            Assert.assertTrue(isWrongAudience);
        }
    }

    /**
     * Simple test to check whether the SSOStringProcessor appropriately fails when the wrong key is used to encrypt the payload
     *
     * @throws InvalidJwtException     is expected
     * @throws MalformedClaimException should not be thrown since it should throw earlier exception
     * @throws JoseException           should not be thrown since it should throw earlier exception
     */
    @Test
    public void testForWrongKey() throws InvalidJwtException, MalformedClaimException, JoseException {
        try {
            JwtClaims claims = unitUnderTest.process(wrongKeyPayload);
        } catch (InvalidJwtException e) {
            boolean isSignatureException = e instanceof InvalidJwtSignatureException;
            Assert.assertTrue(isSignatureException);
        }
    }

    /**
     * Simple test to check whether the SSOStringProcessor appropriately fails when the JWT is expired
     *
     * @throws InvalidJwtException     is expected
     * @throws MalformedClaimException should not be thrown since it should throw earlier exception
     * @throws JoseException           should not be thrown since it should throw earlier exception
     */
    @Test
    public void testForExpiredDate() throws MalformedClaimException, JoseException {
        try {
            JwtClaims claims = unitUnderTest.process(expiredDatePayload);
        } catch (InvalidJwtException e) {
            boolean isExpired = e.getMessage().contains("is on or after the Expiration Time");
            Assert.assertTrue(isExpired);
        }
    }
}
