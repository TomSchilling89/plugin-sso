package com.eyo.sample;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.lang.JoseException;

import java.security.Key;

/**
 * This class is responsible for encrypting payloads for testing purposes.
 * Package private access since it should not be used outside of tests.
 */
class TestPayloadSerialiser {
    private final Key key;

    TestPayloadSerialiser(Key key) {
        this.key = key;
    }

    /**
     * Properly encrypts/serialises a payload for usage in Tests for {@link SSOStringProcessor} and in turn {@link EyoSSO}
     * @param payload the payload that should be serialised
     * @return encrypted/serialised payload in String format
     * @throws JoseException if, for example, the key does not meet the requirements of the encryption algorithm
     */
    public String serialise(String payload) throws JoseException {
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(payload);
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws.setKey(key);
        return jws.getCompactSerialization();
    }
}
