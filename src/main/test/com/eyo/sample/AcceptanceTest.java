package com.eyo.sample;

import com.eyo.sample.exception.DecryptionException;
import com.eyo.sample.model.SSOPayload;
import com.google.common.base.Charsets;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.InvalidJwtSignatureException;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

public class AcceptanceTest {
    private final static String AUDIENCE = "Testing";
    private final static String VALUE_SEPARATOR = "\":\"";
    private final static String ENTRY_SEPARATOR = "\",\"";
    private static final HmacKey KEY = new HmacKey("testtesttesttesttesttesttesttest".getBytes(Charsets.UTF_8));

    private EyoSSO unitUnderTest;

    private Map<String, String> payloadMap = new LinkedHashMap<>();
    private String correctPayload;


    /**
     * Mocks PropertyProvider to return replicable Audience and Key contents for testing purposes
     */
    @Before
    public void mockPropertyProvider() {
        PropertyProvider propertyProvider = Mockito.mock(PropertyProvider.class);
        Mockito.when(propertyProvider.getPluginId()).thenReturn(AUDIENCE);
        Mockito.when(propertyProvider.getPluginKey()).thenReturn(KEY);

        SSOStringProcessor stringProcessor = new SSOStringProcessor(propertyProvider);
        PayloadCreator payloadCreator = new PayloadCreator();

        unitUnderTest = new EyoSSO(stringProcessor, payloadCreator);
    }

    /**
     * Creates payload content and prepares it with {@link TestPayloadSerialiser} for usage in testing
     *
     * @throws JoseException
     */
    @Before
    public void initialisePayloads() throws JoseException {
        TestPayloadSerialiser testPayloadSerialiser = new TestPayloadSerialiser(KEY);
        StringBuilder stringBuilder = new StringBuilder();

        payloadMap.put("eyo.i", "584694cae4b0fd010fa929c6");
        payloadMap.put("eyo.u", "584694f6e4b0fd010fa929ca");
        payloadMap.put("eyo.t", "user");
        payloadMap.put("eyo.ue", "tom");
        payloadMap.put("eyo.ufn", "Tom");
        payloadMap.put("eyo.uln", "Schilling");
        payloadMap.put("eyo.ur", "editor");
        payloadMap.put("eyo.ul", "de_De");

        stringBuilder = stringBuilder.append("{\"")
                .append("aud").append(VALUE_SEPARATOR).append(AUDIENCE).append(ENTRY_SEPARATOR)
                .append("exp").append("\":").append("2481106805").append(",\"")
                .append("iat").append("\":").append("1481106745").append(",\"")
                .append("sub").append(VALUE_SEPARATOR).append("eyosso:584694f6e4b0fd010fa929ca").append(ENTRY_SEPARATOR)
                .append("iss").append(VALUE_SEPARATOR).append("app.eyo.net").append(ENTRY_SEPARATOR)
                .append("eyo.i").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.i")).append(ENTRY_SEPARATOR)
                .append("eyo.u").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.u")).append(ENTRY_SEPARATOR)
                .append("eyo.t").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.t")).append(ENTRY_SEPARATOR)
                .append("eyo.ue").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.ue")).append(ENTRY_SEPARATOR)
                .append("eyo.ufn").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.ufn")).append(ENTRY_SEPARATOR)
                .append("eyo.uln").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.uln")).append(ENTRY_SEPARATOR)
                .append("eyo.ur").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.ur")).append(ENTRY_SEPARATOR)
                .append("eyo.ul").append(VALUE_SEPARATOR).append(payloadMap.get("eyo.ul")).append("\"}");

        String rawPayload = stringBuilder.toString();
        correctPayload = testPayloadSerialiser.serialise(rawPayload);
    }

    @Test
    public void testForAcceptance() throws DecryptionException {
        SSOPayload ssoPayload = unitUnderTest.decrypt(correctPayload);

        Assert.assertEquals(payloadMap.get("eyo.i"), ssoPayload.getInstanceID());
        Assert.assertEquals(payloadMap.get("eyo.u"), ssoPayload.getUserID());
        Assert.assertEquals(payloadMap.get("eyo.ue"), ssoPayload.getUserExternalID());
        Assert.assertEquals(payloadMap.get("eyo.ufn"), ssoPayload.getUserFirstName());
        Assert.assertEquals(payloadMap.get("eyo.uln"), ssoPayload.getUserLastName());
        Assert.assertEquals(payloadMap.get("eyo.ur"), ssoPayload.getUserRole());
        Assert.assertEquals(payloadMap.get("eyo.ul"), ssoPayload.getUserLocale());
    }
}
