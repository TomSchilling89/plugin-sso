package com.eyo.sample;

import com.google.inject.Inject;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

import java.security.Key;

/**
 * Responsible for processing a {@link String} that represents the encrypted Jwt Payload to a {@link JwtClaims} object.
 */
public class SSOStringProcessor {
    private final PropertyProvider propertyProvider;

    @Inject
    public SSOStringProcessor(PropertyProvider propertyProvider) {
        this.propertyProvider = propertyProvider;
    }

    /**
     * @param encryptedSSO the encrypted {@link String} that contains the information for the {@link JwtClaims}
     * @return {@link JwtClaims} that represent the decrypted content of the input {@link String}
     * @throws InvalidJwtException when the presented input cannot be parsed
     */
    public JwtClaims process(String encryptedSSO) throws InvalidJwtException {
        String pluginId = propertyProvider.getPluginId();
        Key pluginKey = propertyProvider.getPluginKey();
        final JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setJwsAlgorithmConstraints(new
                        AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST,
                        AlgorithmIdentifiers.HMAC_SHA256))
                .setVerificationKey(pluginKey)
                .setExpectedAudience(true, pluginId)
                .build();

        return jwtConsumer.processToClaims(encryptedSSO);
    }
}
