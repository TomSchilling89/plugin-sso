package com.eyo.sample;

import com.eyo.sample.exception.DecryptionException;
import com.eyo.sample.model.SSOPayload;

/**
 * Used to segregate the implementation contained in {@link EyoSSO} from the usage and opening up possibility of other decrypting methods.
 */
public interface SSOPayloadDecrypter {
    SSOPayload decrypt(String encryptedSSO) throws DecryptionException;
}
