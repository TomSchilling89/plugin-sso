package com.eyo.sample.model;

import jersey.repackaged.com.google.common.base.Objects;

/**
 * Bean class for holding the eyo SSO payload. Overwritten {@link SSOPayload#toString()} for better formatting.
 */
public class SSOPayload {
    private final String instanceID;
    private final String userID;
    private final String userExternalID;
    private final String userFirstName;
    private final String userLastName;
    private final String userRole;
    private final String userLocale;

    /**
     * Bean constructor used for initialising all bean properties.
     *
     * @param instanceID     ID if plugin instance to access
     * @param userID         ID of the user accessing the plugin
     * @param userExternalID ID of the user accessing the plugin in an external system
     * @param userFirstName  First name of the user accessing the plugin
     * @param userLastName   Last name of the user accessing the plugin
     * @param userRole       Role of the accessing user
     * @param userLocale     Locale of the accessing user
     */
    public SSOPayload(
            final String instanceID,
            final String userID, final String userExternalID, final String userFirstName,
            final String userLastName, final String userRole, final String userLocale) {
        this.instanceID = instanceID;
        this.userID = userID;
        this.userExternalID = userExternalID;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userRole = userRole;
        this.userLocale = userLocale;
    }

    public String getInstanceID() {
        return instanceID;
    }

    public String getUserExternalID() {
        return userExternalID;
    }

    public String getUserID() {
        return userID;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public String getUserRole() {
        return userRole;
    }

    public String getUserLocale() {
        return userLocale;
    }

    /**
     * Overwritten for better formatting
     *
     * @return Concatenated string containing all bean properties of {@link SSOPayload}
     */
    @Override
    public String toString() {
        return Objects.toStringHelper(SSOPayload.class)
                .add("instanceId", getInstanceID())
                .add("userId", getUserID())
                .add("userExternalID", getUserExternalID())
                .add("userFirstName", getUserFirstName())
                .add("userLastName", getUserLastName())
                .add("userRole", getUserRole())
                .add("userLocale", getUserLocale())
                .toString();
    }
}