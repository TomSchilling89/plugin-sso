package com.eyo.sample.injection;

import java.security.Key;

import com.eyo.sample.EyoSSO;
import com.eyo.sample.SSOPayloadDecrypter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jose4j.keys.HmacKey;

import com.google.common.base.Charsets;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

/**
 * Module responsible for declaring {@link EyoSSO} an eager singleton
 */
public class PluginModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(SSOPayloadDecrypter.class).to(EyoSSO.class);
        bind(EyoSSO.class).asEagerSingleton();
    }
}
