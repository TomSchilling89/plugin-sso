package com.eyo.sample;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.eyo.sample.exception.DecryptionException;
import com.eyo.sample.model.SSOPayload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.inject.Inject;

@Path("/")
public class PluginResource {
    private static final Logger logger = LogManager.getLogger(PluginResource.class);

    @Inject
    private SSOPayloadDecrypter payloadDecrypter;

    /**
     * EyoSSO Endpoint called by eyo backend.
     * The "eyosso" query param contains the jwt token.
     *
     * @param httpRequest
     * @param httpResponse
     * @param context
     * @param uriInfo
     * @return
     */
    @GET
    @Path("/eyosso")
    public Response eyosso(
            @Context final HttpServletRequest httpRequest,
            @Context final HttpServletResponse httpResponse,
            @Context final ServletContext context,
            @Context final UriInfo uriInfo) {
        final String encryptedSSO = uriInfo.getQueryParameters().getFirst("eyosso");

        if (logger.isDebugEnabled()) {
            logger.debug("sso started: {}", uriInfo.getRequestUri());
        }

        final SSOPayload ssoPayload;
        try {
            ssoPayload = payloadDecrypter.decrypt(encryptedSSO);

            if (logger.isDebugEnabled()) {
                logger.debug("sso successful: {}", ssoPayload);
            }

            final RequestDispatcher dispatcher = context.getRequestDispatcher("/index.jsp");
            setRequestAttributes(httpRequest, ssoPayload);

            try {
                dispatcher.forward(httpRequest, httpResponse);
                return null;
            } catch (final Exception e) {
                logger.error("Unable to forward request to jsp: {}", e.getMessage(), e);
                return Response.status(500).build();
            }
        } catch (DecryptionException e) {
            logger.error("Unable to use sso service: {}", e.getMessage(), e);
            return forwardError(httpRequest, httpResponse, context);
        }
    }

    private void setRequestAttributes(@Context HttpServletRequest httpRequest, SSOPayload ssoPayload) {
        httpRequest.setAttribute("instanceID", ssoPayload.getInstanceID());
        httpRequest.setAttribute("id", ssoPayload.getUserID());
        httpRequest.setAttribute("externalID", ssoPayload.getUserExternalID());
        httpRequest.setAttribute("firstName", ssoPayload.getUserFirstName());
        httpRequest.setAttribute("lastName", ssoPayload.getUserLastName());
        httpRequest.setAttribute("role", ssoPayload.getUserRole());
        httpRequest.setAttribute("locale", ssoPayload.getUserLocale());
    }

    private Response forwardError(
            final HttpServletRequest httpRequest,
            final HttpServletResponse httpResponse,
            final ServletContext context) {
        final RequestDispatcher dispatcher = context.getRequestDispatcher("/errors/500.jsp");
        try {
            dispatcher.forward(httpRequest, httpResponse);
            return null;
        } catch (final Exception e) {
            logger.error("Unable to forward request to error jsp: {}", e.getMessage(), e);
            return Response.status(500).build();
        }
    }
}
