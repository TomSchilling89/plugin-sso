package com.eyo.sample;

import com.eyo.sample.injection.PluginModule;
import com.google.common.base.Charsets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jose4j.keys.HmacKey;

import java.security.Key;

public class PropertyProvider {
    private static final Logger logger = LogManager.getLogger(PropertyProvider.class);
    private static final String PLUGIN_ID = "eyo_plugin_id";
    private static final String PLUGIN_SECRET = "eyo_plugin_secret";

    private String pluginID;
    private Key pluginKey;

    public String getPluginId() {
        if (pluginID == null) {
            pluginID = getProperty(PLUGIN_ID);
        }

        return pluginID;
    }

    public Key getPluginKey() {
        if (pluginKey == null) {
            final String secret = getProperty(PLUGIN_SECRET);
            pluginKey = new HmacKey(secret.getBytes(Charsets.UTF_8));
        }

        return pluginKey;
    }

    /**
     * Retrieves a property from either the JavaOpts or the Environmental Variables.
     * Logs unavailability and returns default.
     *
     * @param propName Requested Property
     * @return either the value of the property or a default value
     */
    private String getProperty(final String propName) {

        // check java opts
        String propValue = System.getProperty(propName);
        if (propValue != null) {
            logger.debug("Found '{}' in java opts.", propName);
            return propValue;
        }

        // check environmental vars
        propValue = System.getenv(propName);
        if (propValue != null) {
            logger.debug("Found '{}' in environmental variables.", propName);
            return propValue;
        }

        // fallback
        logger.warn("Unable to find '{}'. Please set it in java opts or as "
                + "environmental variable. Eyo SSO will fail.", propName);
        return propName + ".not.set";
    }
}
