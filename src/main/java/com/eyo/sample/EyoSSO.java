package com.eyo.sample;

import com.eyo.sample.exception.DecryptionException;
import com.eyo.sample.model.SSOPayload;
import com.google.inject.Inject;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;

public class EyoSSO implements SSOPayloadDecrypter {
    private final SSOStringProcessor claimsProducer;
    private final PayloadCreator payloadCreator;

    @Inject
    public EyoSSO(SSOStringProcessor stringProcessor, PayloadCreator payloadCreator) {
        this.claimsProducer = stringProcessor;
        this.payloadCreator = payloadCreator;
    }

    /**
     * Decrypts/validates given eyo sso token.
     * Reduced to a Facade for {@link SSOStringProcessor} and {@link PayloadCreator}
     *
     * @param encryptedSSO the encrypted eyo sso token.
     * @return The retrieved payload
     * @throws DecryptionException when the either input string cannot be parsed to a set of {@link JwtClaims} or when the presented {@link JwtClaims} do not contain all necessary data
     */
    @Override
    public SSOPayload decrypt(String encryptedSSO) throws DecryptionException {
        try {
            JwtClaims claims = claimsProducer.process(encryptedSSO);
            return payloadCreator.createPayload(claims);
        } catch (InvalidJwtException e) {
            throw new DecryptionException("The encrypted SSO string could not be parsed.", e);
        } catch (MalformedClaimException e) {
            throw new DecryptionException("The data contained in the SSO string was not complete.", e);
        }
    }
}
