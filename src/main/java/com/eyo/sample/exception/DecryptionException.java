package com.eyo.sample.exception;

public class DecryptionException extends Exception {
    public DecryptionException() {
    }

    public DecryptionException(String message) {
        super(message);
    }

    public DecryptionException(Throwable cause) {
        super(cause);
    }

    public DecryptionException(String message, Throwable cause) {
        super(message, cause);
    }
}
