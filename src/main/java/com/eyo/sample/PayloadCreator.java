package com.eyo.sample;

import com.eyo.sample.model.SSOPayload;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;

/**
 * Responsible for a {@link SSOPayload} from {@link JwtClaims}
 *  Could potentially be cleaned up by using {@link JwtClaims#getClaimsMap()} and using setters which are scanned for to extract the requried names.
 */
public class PayloadCreator {
    /**
     * Creates a {@link SSOPayload} from {@link JwtClaims}
     * @param claims the {@link JwtClaims} that should be "parsed"
     * @return the {@link SSOPayload} that contains the information presented by the {@link JwtClaims}
     * @throws MalformedClaimException when a necessary claim value is not in the correct format within the given {@link JwtClaims}
     */
    public SSOPayload createPayload(JwtClaims claims) throws MalformedClaimException {
        return new SSOPayload(
                claims.getClaimValue("eyo.i", String.class),
                claims.getClaimValue("eyo.u", String.class),
                claims.getClaimValue("eyo.ue", String.class),
                claims.getClaimValue("eyo.ufn", String.class),
                claims.getClaimValue("eyo.uln", String.class),
                claims.getClaimValue("eyo.ur", String.class),
                claims.getClaimValue("eyo.ul", String.class)
        );
    }
}
